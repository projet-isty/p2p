#include "client.h"


CONNECTION_INFO cinfo;

void init_paires_couleurs()
{
	 init_pair(1,COLOR_WHITE,COLOR_BLUE);
	 init_pair(2,COLOR_WHITE,COLOR_RED);
	 init_pair(3,COLOR_WHITE,COLOR_GREEN);
	 init_pair(4,COLOR_BLACK,COLOR_YELLOW);
	 

}

void init_palette_couleurs()
{

	if(has_colors())
	 {
     	start_color();

		init_paires_couleurs();			
		bkgd(COLOR_PAIR(1));
	}		
}

void init_ncurses()
{

	if(initscr() == NULL)
		fprintf(stderr, " L'initialisation de la bibliothèque ncurses a échouée \n\n Merci de réessayer. \n\n"),exit(EXIT_FAILURE);

	cbreak();

	init_palette_couleurs();
	refresh();

}

void init_dimensions_surface_fenetres()
{
	
	menu.largeur_fenetre = COLS - 2, menu.hauteur_fenetre = LINES - 5;
	menu.largeur_formulaire = menu.largeur_fenetre, menu.hauteur_formulaire = LINES - menu.hauteur_fenetre;
	
}

void charge_cadre_fenetres()
{

	box(menu.win,0,0);
	box(menu.form,0,0);

}

void init_surface_fenetres()
{

	init_dimensions_surface_fenetres();
	
	menu.win  = newwin(menu.hauteur_fenetre,menu.largeur_fenetre,1,1);
	menu.form = newwin(menu.hauteur_formulaire - 1,menu.largeur_formulaire,menu.hauteur_fenetre + 1,1);

	charge_cadre_fenetres();

}


void charger_menu()
{
	
	switch(menu.afficher)
  	{
				
		case CONNEXION_SERVEUR_CENTRALISE:
			afficher_menu_connexion();
		break;
		case ENREG_SERVEUR_CENTRALISE:
			afficher_menu_enregistrement();
		break;
		case AIDE1:
			afficher_aide1();
		break;
		case MENU_RECHERCHE:
			afficher_menu_recherche();
		break;	
		default://case MENU_PRINCIPAL:
			afficher_menu_principal();
		break;

	}
	wrefresh(menu.win);
	wrefresh(menu.form);

}



void signal_redimensionner_fenetre(int signal)
{

	redim_fenetre = 1;

}


void redimensionner_fenetre()
{
		
	if(redim_fenetre == 1)
	{
		endwin();

		init_ncurses();
		init_surface_fenetres();

		nettoyer_fenetre(menu.win , menu.form, menu.largeur_fenetre, menu.hauteur_formulaire);

		redim_fenetre = 0;
	}

}



void  afficher_entete(WINDOW *win,int largeur_fenetre)
{

	char *entete = "SYSTEME DE PARTAGE DE FICHIERS - PEER TO PEER";

	wattron(win,A_UNDERLINE | A_STANDOUT | A_BOLD);		
	mvwprintw(win,5,(largeur_fenetre - strlen(entete))/2,"%s",entete);
	wattroff(win,A_UNDERLINE | A_STANDOUT | A_BOLD);

	text_x = (largeur_fenetre - strlen(entete))/2;

}

void affiche_texte_formulaire(WINDOW *form)
{

	mvwprintw(form,1, 3, "Saisir votre requete :  ");
	wrefresh(form);

}



int est_saisie_valide(WINDOW *form , int active_choix_etendu)
{

	int choix = wgetch(form);

	if(!active_choix_etendu)
	{
		if(choix == 'a' || choix == 'b' || choix == 'c' || choix == 'd')
			return choix;
		else
			return '0';

	}

	return choix;


}


void nettoyer_fenetre(WINDOW *win , WINDOW *form, int largeur_fenetre, int hauteur_formulaire)
{

	clear();
	wclear(win);
	wclear(form);

	charge_cadre_fenetres();

	wrefresh(win);
	wrefresh(form);

}

void afficher_aide1()
{
	char choix_utilisateur = 0;
	
	while(choix_utilisateur != 'q')
	{
		redimensionner_fenetre();
		nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
		
		afficher_entete(menu.win,menu.largeur_fenetre);
		affiche_texte_formulaire(menu.form);		


		wattron(menu.win,A_BOLD | A_UNDERLINE);
		mvwprintw(menu.win,text_y - 3,text_x + 10," c- Aide ");
		wattroff(menu.win,A_BOLD | A_UNDERLINE);
	
		mvwprintw(menu.win,text_y,text_x - 5," Pour vous inscrire ou vous connecter au serveur centralisé,  ");
		mvwprintw(menu.win,text_y + 2,text_x - 5," Vous devez saisir l'adresse IP du serveur centralisé, le port (en général 50000), ");
		mvwprintw(menu.win,text_y + 4,text_x - 5 ," Ainsi que vos identifiants  ");
		
		wattron(menu.win,COLOR_PAIR(4));
		mvwprintw(menu.win,text_y + 6,text_x + 10," q- quitter  ");
		wattroff(menu.win,COLOR_PAIR(4));

		wrefresh(menu.win);
		wmove(menu.form,1,30);
		wrefresh(menu.form);
		choix_utilisateur = wgetch(menu.form);
 
	}
	
	nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
	menu.afficher = MENU_PRINCIPAL;
	
}

void afficher_aide2()
{
	char choix_utilisateur = 0;
	
	while(choix_utilisateur != 'q')
	{
		redimensionner_fenetre();
		nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
		
		afficher_entete(menu.win,menu.largeur_fenetre);
		affiche_texte_formulaire(menu.form);		


		wattron(menu.win,A_BOLD | A_UNDERLINE);
		mvwprintw(menu.win,text_y - 3,text_x + 10," c- Aide ");
		wattroff(menu.win,A_BOLD | A_UNDERLINE);
	
		mvwprintw(menu.win,text_y,text_x - 5," Pour rechercher un fichier ou mettre à jour votre liste de fichiers partagés,  ");
		mvwprintw(menu.win,text_y + 2,text_x - 5," Suivez les instructions indiquées sur l'écran, ");
		
		wattron(menu.win,COLOR_PAIR(4));
		mvwprintw(menu.win,text_y + 5,text_x + 10," q- quitter  ");
		wattroff(menu.win,COLOR_PAIR(4));

		wrefresh(menu.win);
		wmove(menu.form,1,30);
		wrefresh(menu.form);
		choix_utilisateur = wgetch(menu.form);
 
	}
	
	nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
	menu.afficher = MENU_PRINCIPAL;
	
}

void afficher_menu_enregistrement()
{
	
	afficher_entete(menu.win,menu.largeur_fenetre);
	affiche_texte_formulaire(menu.form);		


	wattron(menu.win,A_BOLD | A_UNDERLINE);
	mvwprintw(menu.win,text_y - 3,text_x," a- Créer un nouveau compte ");
	wattroff(menu.win,A_BOLD | A_UNDERLINE);


}



void afficher_menu_connexion()
{
	
	afficher_entete(menu.win,menu.largeur_fenetre);
	affiche_texte_formulaire(menu.form);		


	wattron(menu.win,A_BOLD | A_UNDERLINE);
	mvwprintw(menu.win,text_y - 3,text_x," b- Se connecter au serveur centralisé ");
	wattroff(menu.win,A_BOLD | A_UNDERLINE);


}

void afficher_menu_recherche()
{
	
	afficher_entete(menu.win,menu.largeur_fenetre);
	affiche_texte_formulaire(menu.form);		


	wattron(menu.win,A_BOLD | A_UNDERLINE);
	mvwprintw(menu.win,text_y - 3,text_x," a- Rechercher un fichier ");
	wattroff(menu.win,A_BOLD | A_UNDERLINE);


}

int  est_nom_utilisateur_valide(char *nom , char *buffer_erreur)
{
	regex_t regex;
	int rv;

	memset(buffer_erreur,'\0',(size_t) BUFFER_SIZE);
	rv = regcomp(&regex,NOM_UTILISATEUR_REGEX,REG_EXTENDED);

	
	if(rv != 0)
	{

		 regerror(rv,&regex,buffer_erreur,(size_t) BUFFER_SIZE - 10);
		 regfree(&regex);
		 return 0;
	}

	rv = regexec(&regex, nom, 0,NULL,0);

	(rv != 0)? regerror(rv,&regex,buffer_erreur,(size_t) BUFFER_SIZE - 10),regfree(&regex) : 0;

	return !rv;

}


int  est_addresse_valide(char *adresseIP , char *buffer_erreur)
{
	regex_t regex;
	int rv;

	memset(buffer_erreur,'\0',(size_t) BUFFER_SIZE);
	rv = regcomp(&regex,ADDRESSE_IP_REGEX,REG_EXTENDED);

	
	if(rv != 0)
	{

		 regerror(rv,&regex,buffer_erreur,(size_t) BUFFER_SIZE - 10);
		 regfree(&regex);
		 return 0;
	}

	rv = regexec(&regex, adresseIP, 0,NULL,0);

	(rv != 0)? regerror(rv,&regex,buffer_erreur,(size_t) BUFFER_SIZE - 10),regfree(&regex) : 0;

	return !rv;

}


int  est_numero_port_valide(char *port , char *buffer_erreur)
{
	regex_t regex;
	int rv;

	memset(buffer_erreur,'\0',(size_t) BUFFER_SIZE);
	rv = regcomp(&regex,PORT_REGEX,REG_EXTENDED);

	if(rv != 0)
    {
      regerror(rv,&regex,buffer_erreur,(size_t) BUFFER_SIZE - 10);
      regfree(&regex);
	  return 0;
	}

	rv = regexec(&regex, port, 0,NULL,0);

	(rv != 0)? regerror(rv,&regex,buffer_erreur,(size_t) BUFFER_SIZE - 10),regfree(&regex) : 0;

	return !rv;

}

void getInfoServeurDistant(const char *info_serv_distant)
{
	int c = 'A';
	int i = 0;
	int j = 0;
	char port[BUFFER_SIZE] = {'\0'};
	char liste_utilisateur[5000] = {'\0'};
	
	while(c != '-' && c != '\0')
	{
		c = info_serv_distant[i];
		i += 1;
	}
	
	c = 'A';
	
	while(c != '-' && c != '\0')
	{
		c = cinfo.ip_serveur_distant[j] = info_serv_distant[i];
		 
		i += 1;
		j += 1;
	}	
	
	cinfo.ip_serveur_distant[j - 1] = '\0';
	j = 0;
	
	while(c != '\n' && c != '\0')
	{
		c = port[j] = info_serv_distant[i];
		 
		i += 1;
		j += 1;
	}
	
	port[j - 1] = '\0';
	
	cinfo.port_serveur_distant = ((uint16_t) atoi(port));
	
	snprintf(liste_utilisateur,(size_t)3000,"echo '%s-%s' > toto2",cinfo.ip_serveur_distant,port);
	c = system(liste_utilisateur);
}

void rechercher_fichier_distant()
{
	menu.afficher = MENU_RECHERCHE;
	char fichier[DATA_SIZE];
	char reponse[DATA_SIZE];
	char message[BUFFER_SIZE];
	char liste_utilisateur[5000] = {'\0'};
	int est_recherche_active = 0, quitter = 0, rv = 0;
	struct sockaddr_in serveur;
	struct sockaddr_in client;
	char requete[LONGUEUR_CODE] = {'\0'};
	int taille_fichier_desc = 0;
	int offset = 0;
	char *buffer = NULL;
	FILE *F = NULL;
	
	memset(reponse,'\0',(size_t)DATA_SIZE);

	memset(&serveur,0,sizeof(struct sockaddr_in));
	serveur.sin_family = AF_INET;
	serveur.sin_port = htons(cinfo.port_serv_central);
	inet_pton(serveur.sin_family,cinfo.ip_serv_central,&((serveur.sin_addr).s_addr));
	
	memset(&client,0,sizeof client);
	client.sin_family = AF_INET;
	client.sin_port = htons(0);
	
	
	while(!quitter)
	{
		redimensionner_fenetre();
		nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
		
		if(!est_recherche_active)
		{
			charger_menu();
			
			memset(fichier,'\0',(size_t)DATA_SIZE);
			memset(message,'\0',(size_t)BUFFER_SIZE);
			memset(liste_utilisateur,'\0',(size_t)5000);
			
			mvwprintw(menu.win,text_y,text_x," Saisissez le nom du fichier à rechercher  ");
			
			wattron(menu.win,COLOR_PAIR(3));
			mvwprintw(menu.win,text_y + 5,text_x,"Saisissez \" quitter \" pour retourner au menu principal  ");
			wattroff(menu.win,COLOR_PAIR(3));
			
			wrefresh(menu.win);
		    wrefresh(menu.form);
			
			mvwgetnstr(menu.form,1,27,fichier,DATA_SIZE - 1);
			
			if(!strncmp("quitter",fichier,(size_t)7))
				quitter = 1;
			else
			{
				memset(cinfo.status_connection,'\0',BUFFER_SIZE);
				
				memset(reponse,'\0',(size_t)DATA_SIZE);
				memcpy(reponse,fichier,strlen(fichier) + 1);
				
				if(snprintf(message,(size_t) LONGUEUR_CODE,"%s",FILE_LOOKUP) < 0 || snprintf(message + LONGUEUR_CODE,(size_t) NAME_SIZE,"%s",fichier) < 0 || (cinfo.socket_client = socket(AF_INET,SOCK_STREAM, 0)) < 0)
				{
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
				}

				if(bind(cinfo.socket_client, (const struct sockaddr *) &(client),(socklen_t) sizeof client) < 0 || connect(cinfo.socket_client,(const struct sockaddr *) &serveur,(socklen_t) sizeof serveur) == -1)
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
				}
				if(send(cinfo.socket_client,message,(size_t)BUFFER_SIZE, 0) <= 0)
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
				}

				if((rv = recv(cinfo.socket_client,liste_utilisateur,5000, 0)) < 0)
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
					
				}
				else
					est_recherche_active = 1;					
			}
							
		}
		else
		{
			afficher_entete(menu.win,menu.largeur_fenetre);
			affiche_texte_formulaire(menu.form);
			
			if(rv == 0)
			{
				wattron(menu.win,COLOR_PAIR(2));
				mvwprintw(menu.win,text_y,text_x," Le fichier demandé n'existe pas ");
				wattroff(menu.win,COLOR_PAIR(2));
			
				mvwprintw(menu.win,text_y + 5,text_x,"Saisissez \" quitter \" pour retourner au menu principal  ");
			
			}
			else
			{
				//memcpy(reponse,fichier,(size_t)DATA_SIZE);
				
				wattron(menu.win,COLOR_PAIR(3));
				mvwprintw(menu.win,text_y,text_x," Le fichier a été trouvé ");
				wattroff(menu.win,COLOR_PAIR(3));
				
				mvwprintw(menu.win,text_y + 4,text_x + 4,liste_utilisateur);
				
				mvwprintw(menu.win,text_y + 7,text_x,"Saisissez le nom de l'hôte à contacter ou  \" quitter \"  pour retourner au menu principal");
			}
			
			memset(fichier,'\0',(size_t)DATA_SIZE);
			
			wrefresh(menu.win);
		    wrefresh(menu.form);
			
			mvwgetnstr(menu.form,1,27,fichier,DATA_SIZE - 1);
			
			if(!strncmp("quitter",fichier,(size_t)7))
				break;
			if(rv != 0)
			{
				if(send(cinfo.socket_client,fichier,(size_t)DATA_SIZE, 0) <= 0)
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
				}
				
				memset(message,'\0',(size_t)BUFFER_SIZE);
				
				if(recv(cinfo.socket_client,message,(size_t)BUFFER_SIZE, 0) <= 0)
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
					
				}
				
				memset(liste_utilisateur,'\0',(size_t)5000);
				
				getInfoServeurDistant(message);
				
				memset(&serveur,0,sizeof(struct sockaddr_in));
				serveur.sin_family = AF_INET;
				serveur.sin_port = htons(cinfo.port_serveur_distant);
				inet_pton(serveur.sin_family,cinfo.ip_serveur_distant,&((serveur.sin_addr).s_addr));
				
				close(cinfo.socket_client);
				memset(&client,0,sizeof client);
				client.sin_family = AF_INET;
				client.sin_port = htons(0);
				
				if((cinfo.socket_client = socket(AF_INET,SOCK_STREAM, 0)) < 0)
				{
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
				}

				if(bind(cinfo.socket_client, (const struct sockaddr *) &(client),(socklen_t) sizeof client) < 0 )
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
				}
				

				snprintf(liste_utilisateur,(size_t) LONGUEUR_CODE,"%s",GET_FILE);
				snprintf(liste_utilisateur + LONGUEUR_CODE,(size_t) DATA_SIZE,"%s",reponse);
				
				if(connect(cinfo.socket_client,(const struct sockaddr *) &serveur,(socklen_t) sizeof serveur) == -1)
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Le serveur distant est inaccessible") + 1,"Le serveur distant est inaccessible");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
					
				}
				if(send(cinfo.socket_client,liste_utilisateur,(size_t)4000, 0) <= 0)
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
				}
				
				memset(requete,'\0',(size_t)LONGUEUR_CODE);
				
				if((rv = (int)recv(cinfo.socket_client,requete,(size_t)LONGUEUR_CODE, 0)) <= 0)
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
					menu.afficher = MENU_PRINCIPAL;
					nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
					return;
				}
							
				taille_fichier_desc = atoi(requete);
								
				if(taille_fichier_desc > 0)
				{
					if(!(buffer = calloc((size_t)taille_fichier_desc,sizeof(char))))
					{
						close(cinfo.socket_client);
						snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
						menu.afficher = MENU_PRINCIPAL;
						nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
						return;
					}	
					if(send(cinfo.socket_client,OKI,strlen(OKI), 0) <= 0)
					{
						free(buffer);
						close(cinfo.socket_client);
						snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
						menu.afficher = MENU_PRINCIPAL;
						nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
						return;
					}
					
					offset = 0;
					
					while(offset < taille_fichier_desc)
					{
						rv = (int) recv(cinfo.socket_client,buffer+offset,(size_t)1, 0);
						
						if(rv < 0)
						{
							free(buffer);
							close(cinfo.socket_client);
							snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
							menu.afficher = MENU_PRINCIPAL;
							nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
							return;
						}
						
						offset += 1;
					}
									
					memset(message,0,(size_t)BUFFER_SIZE);
					snprintf(message,(size_t)DATA_SIZE,"./fichiers_partages/%s",reponse);
					
					if(!(F = fopen(message,"wb+")))
					{
						free(buffer);
						close(cinfo.socket_client);
						snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
						menu.afficher = MENU_PRINCIPAL;
						nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
						return;
					}
					
					if(fwrite(buffer,(size_t)taille_fichier_desc,1,F) == 0)
					{
						free(buffer);
						close(cinfo.socket_client);
						fclose(F);
						snprintf(cinfo.status_connection, strlen("Une erreur est survenue , merci de réessayer") + 1,"Une erreur est survenue , merci de réessayer");
						menu.afficher = MENU_PRINCIPAL;
						nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
						return;
					}
					else
					{
						free(buffer);
						close(cinfo.socket_client);
						fclose(F);
						snprintf(cinfo.status_connection, strlen("Le fichier a été téléchargé , regarder votre dossier de fichiers partagés") + 1,"Le fichier a été téléchargé , regarder votre dossier de fichiers partagés");
						quitter = 1;
					}
				
				}
				else
				{
					close(cinfo.socket_client);
					snprintf(cinfo.status_connection, strlen("Le fichier demandé n'est pas disponible, merci de réessayer") + 1,"Le fichier demandé n'est pas disponible, merci de réessayer");
					quitter = 1;
				}
				
			}
				
			
		}
	}
	
	menu.afficher = MENU_PRINCIPAL;
	nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
}

void traite_requete_utilisateur(const char *requete_utilisateur)
{

	int tube_fd;
	int rv_ip = 1, rv_port = 1,rv_nom = 1, rv_mdp = 1;
	int est_ip_valide = 0, est_port_valide = 0, est_nom_valide = 0, est_mdp_valide = 0;
	char port[BUFFER_SIZE] , nom[NAME_SIZE] , mdp[PASSWD_SIZE], buffer_erreur[BUFFER_SIZE];
	char message[TAILLE_MESSAGE_CONNEXION];
	int status_conn_ok;
	char temp[4] = {'\0'};

	memset(cinfo.code_requete,'\0',LONGUEUR_CODE);
	memset(cinfo.status_connection,'\0',BUFFER_SIZE);
	
	if(!strncmp(SIGN_UP,requete_utilisateur, strlen(SIGN_UP)))
		menu.afficher = ENREG_SERVEUR_CENTRALISE,snprintf(cinfo.code_requete, strlen(SIGN_UP) + 1, SIGN_UP);
	else if(!strncmp(LOG_IN,requete_utilisateur, strlen(LOG_IN)))
		menu.afficher = CONNEXION_SERVEUR_CENTRALISE,snprintf(cinfo.code_requete, strlen(LOG_IN) + 1, LOG_IN);
	
	if((tube_fd = open("/tmp/tube",O_RDWR)) == -1)
	{
		fprintf(stderr,"\n L'ouverture du tube nommé a échoué : %s \n\n",strerror(errno));
		menu.afficher = MENU_PRINCIPAL;
		return;
	}

		
	while(!est_ip_valide || !est_port_valide || !est_nom_valide || !est_mdp_valide)
    {
		redimensionner_fenetre();
		nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);
		charger_menu();
		
		if(!rv_ip)
		{

			wattron(menu.win,COLOR_PAIR(2));
			mvwprintw(menu.win,text_y + 7,text_x + 4," ADRESSE IP : %s \n\n",buffer_erreur);
			wattroff(menu.win,COLOR_PAIR(2));
		}

		if(!est_ip_valide)
        {
		   
			memset(cinfo.ip_serv_central,'\0',(size_t) BUFFER_SIZE);

			wattron(menu.win,A_BOLD);
			mvwprintw(menu.win,text_y + 2,text_x,"  Merci de saisir une adresse IP valide \n\n");
			wattroff(menu.win,A_BOLD);

		    wrefresh(menu.win);
		    wrefresh(menu.form);

			mvwgetnstr(menu.form,1,27,cinfo.ip_serv_central,BUFFER_SIZE - 1);

			est_ip_valide = rv_ip = est_addresse_valide(cinfo.ip_serv_central ,buffer_erreur);
		}
		else if(!est_port_valide)
		{					
			if(!rv_port)
			{
		
				wattron(menu.win,COLOR_PAIR(2));
				mvwprintw(menu.win,text_y + 7,text_x + 4," PORT : %s \n\n",buffer_erreur);
				wattroff(menu.win,COLOR_PAIR(2));
			}

			memset(port,'\0',(size_t) BUFFER_SIZE);

			wattron(menu.win,A_BOLD);
			mvwprintw(menu.win,text_y + 2,text_x,"  Merci de saisir un numéro de port valide \n\n");
			wattroff(menu.win,A_BOLD);

			wrefresh(menu.win);
			wrefresh(menu.form);

			mvwgetnstr(menu.form,1,27,port,BUFFER_SIZE - 1);

			est_port_valide = rv_port = est_numero_port_valide(port,buffer_erreur);

		}

		else if(!est_nom_valide)
		{
			
			if(!rv_nom)
			{
		
				wattron(menu.win,COLOR_PAIR(2));
				mvwprintw(menu.win,text_y + 7,text_x + 4," NOM : %s \n\n",buffer_erreur);
				wattroff(menu.win,COLOR_PAIR(2));
			}

			memset(nom,'\0',(size_t) NAME_SIZE);

			wattron(menu.win,A_BOLD);
			mvwprintw(menu.win,text_y + 2,text_x,"  Merci de saisir un nom valide \n\n");
			wattroff(menu.win,A_BOLD);

			wrefresh(menu.win);
			wrefresh(menu.form);

			mvwgetnstr(menu.form,1,27,nom,NAME_SIZE - 1);

			est_nom_valide = rv_nom = est_nom_utilisateur_valide(nom,buffer_erreur);
			
		}
		else
		{

			if(!rv_mdp)
			{
		
				wattron(menu.win,COLOR_PAIR(2));
				mvwprintw(menu.win,text_y + 7,text_x + 4," Votre mot de passe doit être d'une longueur de 4 caractères \n\n");
				wattroff(menu.win,COLOR_PAIR(2));
			}

			memset(mdp,'\0',(size_t) PASSWD_SIZE);

			wattron(menu.win,A_BOLD);
			mvwprintw(menu.win,text_y + 2,text_x,"  Merci de saisir un mot de passe valide \n\n");
			wattroff(menu.win,A_BOLD);

			wrefresh(menu.win);
			wrefresh(menu.form);

			mvwgetnstr(menu.form,1,27,mdp,4);
	
			if(strlen(mdp) == 4)
				rv_mdp = est_mdp_valide = 1;
			else
				rv_mdp = est_mdp_valide = 0;

		}
	}

	
	cinfo.port_serv_central = (uint16_t) atoi(port);

	memset(message,'\0',TAILLE_MESSAGE_CONNEXION);
	
	snprintf(message, (size_t) LONGUEUR_CODE,"%s", cinfo.code_requete);
	snprintf(message + LONGUEUR_CODE, (size_t) BUFFER_SIZE,"%s", cinfo.ip_serv_central);
	snprintf(message + LONGUEUR_CODE + BUFFER_SIZE, (size_t) BUFFER_SIZE, "%s",port);
	snprintf(message + LONGUEUR_CODE + BUFFER_SIZE + BUFFER_SIZE, (size_t) NAME_SIZE, "%s",nom);
	snprintf(message + LONGUEUR_CODE + BUFFER_SIZE + BUFFER_SIZE + NAME_SIZE, (size_t) PASSWD_SIZE,"%s", mdp);
	
	menu.afficher = MENU_PRINCIPAL;

	if(write(tube_fd,message,(size_t) TAILLE_MESSAGE_CONNEXION) < 0)
	{
		fprintf(stderr,"\n L'écriture dans le tube nommé a échoué : %s \n\n",strerror(errno));
		return;
	}	
	if(read(tube_fd,temp,sizeof(*temp)) < 0)
	{	
		fprintf(stderr,"\n La lecture du tube nommé a échoué : %s \n\n",strerror(errno));
		return;
	}		

	status_conn_ok = atoi(temp);
	
	if(!status_conn_ok)
		menu.afficher_menu_etendu = 0,snprintf(cinfo.status_connection, strlen(ECHEC_CONNEXION_SERVEUR_CENTRALISE) + 1, ECHEC_CONNEXION_SERVEUR_CENTRALISE);
	else
		menu.afficher_menu_etendu = 1,snprintf(cinfo.status_connection, strlen(SUCCES_CONNEXION) + 1, SUCCES_CONNEXION);	
		
	close(tube_fd);	

	nettoyer_fenetre(menu.win,menu.form,menu.largeur_fenetre,menu.hauteur_formulaire);

}


void traite_choix_utilisateur(int choix_utilisateur, int active_choix_etendu)
{
	if(!active_choix_etendu)
	{
		switch(choix_utilisateur)
		{
			
			case 'a':
				traite_requete_utilisateur(SIGN_UP);
			break;
			case 'b':
				traite_requete_utilisateur(LOG_IN);
			break;
			case 'c':
				afficher_aide1();
			break;	
			default:
			break;


		}
		
	}
	else
	{
		switch(choix_utilisateur)
		{
			
			case 'a':
				rechercher_fichier_distant();
			break;
			/*case 'b':
				traite_requete_utilisateur(LOG_IN);
			break;*/
			case 'c':
				afficher_aide2();
			break;	
			default:
			break;


		}
		
	}
	
}


void afficher_menu_principal()
{

	redimensionner_fenetre();

	afficher_entete(menu.win,menu.largeur_fenetre);

	if(menu.afficher_menu_etendu == 0)
	{

		wattron(menu.win,A_BOLD);

		wattron(menu.win,COLOR_PAIR(2));
		mvwprintw(menu.win,text_y - 3,text_x,"%s \n\n",cinfo.status_connection);
		wattroff(menu.win,COLOR_PAIR(2));

		mvwprintw(menu.win,text_y - 1,text_x,"a- Créer un nouveau compte ");
		mvwprintw(menu.win,text_y + 1,text_x,"b- Se connecter au serveur centralisé ");
		mvwprintw(menu.win,text_y + 3 ,text_x,"c- Aide \n\n");
		mvwprintw(menu.win,text_y + 5,text_x,"d- quitter \n\n");
		wattroff(menu.win,A_BOLD);
	}
	else
	{

		wattron(menu.win,A_BOLD);

		wattron(menu.win,COLOR_PAIR(3));
		mvwprintw(menu.win,text_y - 3,text_x,"%s \n\n",cinfo.status_connection);
		wattroff(menu.win,COLOR_PAIR(3));

		mvwprintw(menu.win,text_y - 1,text_x,"a- Rechercher un fichier ");
		mvwprintw(menu.win,text_y + 1,text_x,"b- Mettre à jour le dossier de fichiers partagés ");
		mvwprintw(menu.win,text_y + 3 ,text_x,"c- Aide \n\n");
		mvwprintw(menu.win,text_y + 5,text_x,"d- quitter \n\n");
		wattroff(menu.win,A_BOLD);
	}	
	

	affiche_texte_formulaire(menu.form);

	wrefresh(menu.win);
	wrefresh(menu.form);

}


int main()
{
	
	if(mkfifo("/tmp/tube",0644) == -1 && errno == EEXIST)
		if(unlink("/tmp/tube") == -1 || mkfifo("/tmp/tube",0644) == -1)
			fprintf(stderr,"\n La création du tube nommé a échoué : %s \n\n",strerror(errno)) , exit(EXIT_FAILURE);
			
	pid_t fils = fork();		
	
	switch(fils)
	{
		case 0:
			init_serveur();
			exit(EXIT_SUCCESS);
		break;
		case -1:
			fprintf(stderr,"\n L'initialisation du serveur a échoué (client) : %s \n\n",strerror(errno)) , exit(EXIT_FAILURE);
		break;
		default:
		break;
	}
	
	
	int choix_utilisateur = '\0';
	struct sigaction act;
	
	memset(&menu,0,sizeof(MENU));
	menu.afficher = MENU_PRINCIPAL;
	
	memset(&cinfo,0,sizeof(CONNECTION_INFO));
	
	memset(&act,0,sizeof act);
	act.sa_handler = signal_redimensionner_fenetre;

    if(sigaction(SIGWINCH,&act,NULL) == -1)
		 fprintf(stderr, "sigaction() : %d \nle redimensionnement de la fenêtre a échoué \n\n",errno) , exit(EXIT_FAILURE);

	init_ncurses();
	init_surface_fenetres();
	
	
	while(choix_utilisateur != 'd')
	{
		
		charger_menu();

		choix_utilisateur = est_saisie_valide(menu.form,menu.afficher_menu_etendu);

		traite_choix_utilisateur(choix_utilisateur,menu.afficher_menu_etendu);

		wmove(menu.form,1,30);
		wrefresh(menu.form);
 
	}
	

	endwin();

	unlink("/tmp/tube");

	return 0;

}
