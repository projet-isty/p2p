
#ifndef SERVEUR
#define SERVEUR


#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <regex.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <crypt.h>
#include <dirent.h>
#include <sys/sendfile.h>


#define NAME_SIZE 13
#define PASSWD_SIZE 13
#define ENCRYPTED_PASSWD_SIZE 13
#define DATA_SIZE 64
#define BUFFER_SIZE 256
#define LONGUEUR_CODE 20
#define TAILLE_MESSAGE_CONNEXION (BUFFER_SIZE + BUFFER_SIZE + LONGUEUR_CODE + NAME_SIZE + PASSWD_SIZE)


#define ECHEC_CONNEXION_SERVEUR_CENTRALISE " La connexion au serveur centralisé a échouée , veuillez vérifier vos identifiants ou votre connexion "
#define SUCCES_CONNEXION " Vous êtes maintenant connecté au serveur centralisé "


#define SIGN_UP_OK "SIGN_UP_OK"
#define SIGN_UP_NOT_OK "SIGN_UP_NOT_OK"
#define LOG_IN_OK "LOG_IN_OK"
#define LOG_IN_NOT_OK "LOG_IN_NOT_OK"
#define SND_DESC_OK "SND_DESC_OK"
#define SND_DESC_NOT_OK "SND_DESC_NOT_OK"
#define OKI "OKI"
#define NOT_OK "NOT_OK"

#define SIGN_UP "SIGN_UP"
#define LOG_IN  "LOG_IN"
#define SND_DESC "SND_DESC"
#define GET_FILE "GET_FILE"

typedef struct
{
	int sockfd;
	struct sockaddr_in addr_serveur;
		
}INFO_SERVEUR;

char *chiffrer_mdp(char *mdp);
void init_serveur();
int connexion_serveur_centralise(char *adresse_serv_central, char *port_serv_central, char *requete, char *nom , char *mdp, struct sockaddr_in *addr, INFO_SERVEUR *serveur);

#endif
